# Copyright (c) 2021-2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/test.gni")
import("../../../../../appexecfwk.gni")
import("../../../../../services/bundlemgr/appexecfwk_bundlemgr.gni")

config("private_config") {
  include_dirs = [
    "${services_path}/bundlemgr/test/mock/include",
    "${services_path}/test/moduletest/utils/include",
  ]
}

module_output_path = "bundle_framework/bundlemgrservice"

ohos_unittest("BmsBundleUpdaterTest") {
  use_exceptions = true
  module_out_path = module_output_path
  include_dirs = [ "//third_party/jsoncpp/include" ]
  sources = bundle_mgr_source
  sources -= [ "${services_path}/bundlemgr/src/system_ability_helper.cpp" ]
  sources += [
    "${services_path}/bundlemgr/src/installd/installd_host_impl.cpp",
    "${services_path}/bundlemgr/src/installd/installd_operator.cpp",
    "${services_path}/bundlemgr/src/installd/installd_service.cpp",
  ]

  sources += [
    "${services_path}/bundlemgr/test/mock/src/accesstoken_kit.cpp",
    "${services_path}/bundlemgr/test/mock/src/installd_permission_mgr.cpp",
    "${services_path}/bundlemgr/test/mock/src/mock_status_receiver.cpp",
    "${services_path}/bundlemgr/test/mock/src/system_ability_helper.cpp",
    "${services_path}/test/moduletest/utils/src/common_tool.cpp",
  ]

  sources += bundle_install_sources

  sources += [ "bms_bundle_updater_test.cpp" ]

  configs = [
    ":private_config",
    "${services_path}/bundlemgr/test:bundlemgr_test_config",
    "//base/notification/eventhandler/frameworks/eventhandler:libeventhandler_config",
  ]
  cflags = []
  if (target_cpu == "arm") {
    cflags += [ "-DBINDER_IPC_32BIT" ]
  }
  deps = [
    "${appexecfwk_path}/test/sceneProject/unittest/ohos_test:copy_ohos_test",
    "${appexecfwk_path}/test/sceneProject/unittest/test_bundle/fifth_right:fifth_right",
    "${appexecfwk_path}/test/sceneProject/unittest/test_bundle/first_right:first_right",
    "${appexecfwk_path}/test/sceneProject/unittest/test_bundle/fourth_right:fourth_right",
    "${appexecfwk_path}/test/sceneProject/unittest/test_bundle/rightTest:rightTest",
    "${appexecfwk_path}/test/sceneProject/unittest/test_bundle/rightTest1:rightTest1",
    "${appexecfwk_path}/test/sceneProject/unittest/test_bundle/second_right:second_right",
    "${appexecfwk_path}/test/sceneProject/unittest/test_bundle/seventh_right:seventh_right",
    "${appexecfwk_path}/test/sceneProject/unittest/test_bundle/third_right:third_right",
    "${appexecfwk_path}/test/sceneProject/unittest/test_bundle/versionTest1:versionTest1",
    "${appexecfwk_path}/test/sceneProject/unittest/test_bundle/versionTest2:versionTest2",
    "${appexecfwk_path}/test/sceneProject/unittest/test_bundle/versionTest3:versionTest3",
    "${appexecfwk_path}/test/sceneProject/unittest/test_bundle/wrong_bundle_name:wrong_bundle_name",
  ]

  if (bundle_framework_graphics) {
    include_dirs += [
      "//foundation/multimedia/image_framework/interfaces/innerkits/include",
    ]
    deps += [ "//foundation/multimedia/image_framework/interfaces/innerkits:image_native" ]
  }

  deps += bundle_install_deps

  external_deps = [
    "ability_base:want",
    "access_token:libtokenid_sdk",
    "appverify:libhapverify",
    "bundle_framework:appexecfwk_core",
    "common_event_service:cesfwk_innerkits",
    "eventhandler:libeventhandler",
    "hitrace_native:hitrace_meter",
    "init:libbegetutil",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
    "storage_service:storage_manager_sa_proxy",
  ]
  defines = []
  if (configpolicy_enable) {
    external_deps += [ "config_policy:configpolicy_util" ]
    defines += [ "CONFIG_POLOCY_ENABLE" ]
  }
  if (build_selinux) {
    external_deps += [ "selinux:libhap_restorecon" ]
    cflags += [ "-DWITH_SELINUX" ]
  }
  if (account_enable) {
    external_deps += [ "os_account:os_account_innerkits" ]
    defines += [ "ACCOUNT_ENABLE" ]
  }
  if (bundle_framework_free_install) {
    sources += aging
    sources += free_install
    sources += distributed_manager
    external_deps += [
      "ability_runtime:ability_manager",
      "ability_runtime:app_manager",
      "battery_manager:batterysrv_client",
      "device_info_manager:distributed_device_profile_client",
      "device_usage_statistics:usagestatsinner",
      "display_manager:displaymgr",
      "power_manager:powermgr_client",
      "syscap_codec:syscap_interface_shared",
    ]
    defines += [ "BUNDLE_FRAMEWORK_FREE_INSTALL" ]
  }
  if (device_manager_enable) {
    sources += [ "${services_path}/bundlemgr/src/bms_device_manager.cpp" ]
    external_deps += [ "device_manager:devicemanagersdk" ]
    defines += [ "DEVICE_MANAGER_ENABLE" ]
  }
  if (global_resmgr_enable) {
    defines += [ "GLOBAL_RESMGR_ENABLE" ]
    external_deps += [ "resource_management:global_resmgr" ]
  }
  if (hicollie_enable) {
    external_deps += [ "hicollie_native:libhicollie" ]
    defines += [ "HICOLLIE_ENABLE" ]
  }

  if (hisysevent_enable) {
    sources += [ "${services_path}/bundlemgr/src/inner_event_report.cpp" ]
    external_deps += [ "hisysevent_native:libhisysevent" ]
    defines += [ "HISYSEVENT_ENABLE" ]
  }

  configs += [ "../../../../../services/bundlemgr:rdb_config" ]
  external_deps += [ "relational_store:native_rdb" ]
  sources += [
    "${services_path}/bundlemgr/src/bundle_data_storage_rdb.cpp",
    "${services_path}/bundlemgr/src/preinstall_data_storage_rdb.cpp",
    "${services_path}/bundlemgr/src/rdb/bms_rdb_open_callback.cpp",
    "${services_path}/bundlemgr/src/rdb/rdb_data_manager.cpp",
  ]
}

group("unittest") {
  testonly = true

  if (ability_runtime_enable) {
    deps = [ ":BmsBundleUpdaterTest" ]
  }
}
